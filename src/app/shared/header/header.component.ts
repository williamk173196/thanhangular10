import { Component, OnInit } from '@angular/core';
import { SelectService } from 'src/app/core/services/common/select.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  subscription: Subscription;
  constructor(private _selectService: SelectService) { }

  ngOnInit() {
    setTimeout(() => {
      this._selectService.notifyMenu("Home first load");
    }, 100)
  }

  ClickHeaderMenu(name: string) {
    setTimeout(() => this._selectService.notifyMenu(name), 100);
  }
}
