export class SYSTEM_CONSTANT {
  public static USER_CURRENT: "USER_CURRENT";
  public static API_URL: 'https://localhost:44369';
  public static pageIndex: number = 1;
  public static pageSize: number = 10;
  public static pageDisplay: number = 5;
};
