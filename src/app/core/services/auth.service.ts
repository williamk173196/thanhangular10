import { Router } from "@angular/router";
import { SystemConstant } from "../common/system.constant";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SYSTEM_CONSTANT } from "../constants/system.constant";

const headers = new HttpHeaders({ "content-type": "application/json" });

@Injectable({
    providedIn: "root"
})
export class AuthService {
    public redirectUrl: string;

    constructor(private httpClient: HttpClient, private router: Router) {}
    public login(tenDangNhap: string, matKhau: string,rememberme = true):Observable<any> {
        const model = {
            userName:tenDangNhap,
            password:matKhau,
            rememberMe:rememberme
        }
        let url = SYSTEM_CONSTANT.API_URL + `/api/Login`;
        return this.httpClient.post(url, model,{ headers: headers });
    }

    public logout() {
        localStorage.removeItem(SYSTEM_CONSTANT.USER_CURRENT);
    }

    public checkLoggedInUser(): boolean {
        return localStorage.getItem(SYSTEM_CONSTANT.USER_CURRENT)
            ? true
            : false;
    }
}
