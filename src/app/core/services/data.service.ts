import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SystemConstant } from "../common/system.constant";
import { HttpClient,HttpHeaders } from '@angular/common/http';
const headers = new HttpHeaders({ "Content-Type": "application/json" });

@Injectable()
export class DataService {

  constructor(private _http: HttpClient) {}
  public baseUrl = SystemConstant.BASE_API;

  get(uri: string): Observable<any> {
    return this._http.get(this.baseUrl + uri, {
      headers: headers,
    });
  }

  post(uri: string, data? : any): Observable<any> {
    return this._http.post(this.baseUrl + uri, JSON.stringify(data), {
      headers: headers,
    });
  }

  put(uri: string, data? : any): Observable<any> {
    return this._http.put(this.baseUrl + uri, JSON.stringify(data), {
      headers: headers,
    });
  }

  delete(uri: string, key: string, value: string): Observable<any> {
    return this._http.delete(this.baseUrl + uri + "?" + key + "=" + value, {
      headers: headers,
    });
  }
}
