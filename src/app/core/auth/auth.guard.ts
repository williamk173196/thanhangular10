import { URL_ROLE } from "./../constants/url.constant";
import { Injectable } from "@angular/core";
import {
    CanActivate,
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    NavigationExtras,
    Route,
    CanActivateChild
} from "@angular/router";
import { SYSTEM_CONSTANT } from "../constants/system.constant";
import { AuthService } from "../services/auth.service";

@Injectable({
    providedIn: "root"
})
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private authService: AuthService) {}

    public canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean {
        let url: string = state.url;
        return this.checkLogin(url);
    }

    // public canActivateChild(
    //     route: ActivatedRouteSnapshot,
    //     state: RouterStateSnapshot
    // ): boolean {
    //     let url = `/${state.url}`;
    //     return this.checkLoadChildren(url);
    // }

    // public canLoad(route: Route): boolean {
    //     let url = `/${route.path}`;
    //     return this.checkLoadChildren(url);
    // }

    checkLogin(url: string) {
        if (localStorage.getItem(SYSTEM_CONSTANT.USER_CURRENT)) {
            return true;
        }

        // let sessionId = 123456789;
        // this.authService.redirectUrl = url;
        // let navigationExtrax: NavigationExtras = {
        //     queryParams: { session_id: sessionId },
        //     fragment: "anchor"
        // };
        // , navigationExtrax
        this.router.navigate(["/login"]);
        return false;
    }
}
