import { createReducer, on } from "@ngrx/store";
import { BookDetail } from "../book-interface/book-api.interface";
import { CreateBook, UpdateBook, DeleteBook } from "./book.actions";


export const initialState: BookDetail[] = [];

export const collectionReducer = createReducer(
  initialState,
  on(DeleteBook, (state, {bookId}) => (state.filter(x=>x.id !== bookId)))
)
