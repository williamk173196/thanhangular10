import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BookApiComponent } from "./book-api/book-api.component";
import { NgrxstoreComponent } from "./ngrxstore.component";

const route: Routes = [
  { path: '',  redirectTo: 'book-ngrxstore', pathMatch: 'full'},
  { path: 'ngrxstore', component: NgrxstoreComponent },
  { path: 'book-ngrxstore', component: BookApiComponent }
]

@NgModule({
  imports: [RouterModule.forChild(route)],
  exports: [RouterModule]
})

export class NgrxStoreRouterModule {}
