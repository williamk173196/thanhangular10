import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgrxstoreComponent } from "./ngrxstore.component";
import { NgrxStoreRouterModule } from "./ngrxstore.router";
import { StoreModule } from "@ngrx/store";
import { counterReducer } from "./counter.reducer";
import { MyCounterModule } from "./my-counter/my-counter.module";
import { bookReducer } from "./book-api/book-state/book.reducer";
import { collectionReducer } from "./book-api/book-state/book-collection.reducer";
@NgModule({
  imports: [
    CommonModule,
    NgrxStoreRouterModule,
    // StoreModule.forRoot({count: counterReducer}),
    StoreModule.forRoot({ books: bookReducer, collections: collectionReducer }),
    MyCounterModule
  ],
  declarations: [NgrxstoreComponent]
})

export class NgrxStoreModule {}
