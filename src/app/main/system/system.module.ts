import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { SystemRouterModule } from "./system.router";
import { FunctionComponent } from './function/function.component';

@NgModule({
  imports: [
    CommonModule,
    SystemRouterModule
  ],
  declarations: [FunctionComponent]
})

export class SystemModule {}
