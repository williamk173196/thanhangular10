import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularDraggableModule } from 'angular2-draggable';
import { NgBusyModule } from 'ng-busy';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BookmanagerComponent } from './bookmanager.component';
import { BookManagerRouterModule } from './bookmanager.router';
import { AuthorsComponent } from './authors/authors.component';
import { BooksComponent } from './books/books.component';
import { ModalAuthorsModule } from 'src/app/shared/myapi/bookmanager/modal-authors/modal-authors.module';
import { NotificationService } from 'src/app/core/services/notification.service';
import { BooksauthorsComponent } from './booksauthors/booksauthors.component';
import { ModalBooksModule } from 'src/app/shared/myapi/bookmanager/modal-books/modal-books.module';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BookManagerRouterModule,
    TabsModule.forRoot(),
    AngularDraggableModule,
    NgBusyModule,
    PaginationModule,
    ModalAuthorsModule,
    ModalBooksModule,
    NgSelectModule
  ],
  declarations: [BookmanagerComponent, AuthorsComponent, BooksComponent, BooksauthorsComponent],
  providers: [NotificationService]
})

export class BookManagerModule {}
