import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bookmanager',
  templateUrl: './bookmanager.component.html',
  styleUrls: ['./bookmanager.component.css']
})
export class BookmanagerComponent implements OnInit {
  public getSaveAuthor: any;

  constructor() { }

  ngOnInit(): void {
  }

  GetSaveAuthor(event) {
    this.getSaveAuthor = event;
  }
}
