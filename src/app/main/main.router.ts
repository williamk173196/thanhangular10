import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main.component';
import { SchoolmanagerComponent } from './schoolmanager/schoolmanager.component';
import { SystemComponent } from './system/system.component';

export const mainRoutes: Routes = [
    {
        path: '', component: MainComponent, children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'myapi', loadChildren: () => import("../main/myapi/myapi.module").then(m => m.MyApiModule)},
            { path: 'ngrx', loadChildren: () => import("../main/ngrxstore/ngrxstore.module").then(m => m.NgrxStoreModule)},
            { path: 'system', loadChildren: () => import("../main/system/system.module").then(m => m.SystemModule)},
            { path: 'learnrxjs', loadChildren: () => import("../main/learnrxjs/learnrxjs.module").then(m => m.LearnRxjsModule)},
        ]
    }
]
