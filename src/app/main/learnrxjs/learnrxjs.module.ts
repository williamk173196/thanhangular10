import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LearnRxjsRouterModule } from "./learnrxjs.router";

@NgModule({
  imports: [
    CommonModule,
    LearnRxjsRouterModule,
  ],
  declarations: []
})

export class LearnRxjsModule { }
