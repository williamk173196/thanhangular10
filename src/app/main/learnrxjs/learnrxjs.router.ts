import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LearnrxjsComponent } from "./learnrxjs.component";

const routes: Routes = [
  { path: "", redirectTo: "learnrxjs", pathMatch: "full" },
  { path: "learnrxjs", component: LearnrxjsComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class LearnRxjsRouterModule { }
